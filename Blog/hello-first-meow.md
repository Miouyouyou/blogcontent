+++

categories = ["First note"]
description = "this site description"
tags = ["site", "description", "main note"]
title = "Greetings everyone"
date = "2019-05-08"

+++

This is my main website, where I'm gathering various posts
I wrote here and there, along with some documentation I wrote too.

Kernel code snippets are licensed under GPLv2.  
The rest is licensed under MIT.  
The whole blog content is available on [Gitlab](https://gitlab.com/Miouyouyou/blogcontent).

The Hugo theme that I'm using, Myynimalist, is made by myself and
available on [Gitlab](https://gitlab.com/Miouyouyou/myynimalist).  
This theme is still not very mobile-friendly. Notably the header.  
Still, if you have trouble navigation this site (but can still see
this), [file an issue on Gitlab](https://gitlab.com/Miouyouyou/myynimalist/issues).

I'm still gathering my content, so some old posts might be added.
