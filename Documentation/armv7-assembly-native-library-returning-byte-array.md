+++

categories = ["Android", "ARMv7", "Assembly", "JNI", "Documentation"]
date = "2016-06-25"
description = "write an armv7 native library in assembly which return a java byte[] array to a java android activity"
tags = ["android", "armv7", "native library", "assembly", "jni"]
title = "How to write a native library in ARMv7 assembly, call it from an Android Activity and get back a Java byte[] array"
imported = ["ARM Community", "https://community.arm.com/developer/tools-software/oss-platforms/b/android-blog/posts/example-of-calling-the-jni-directly-from-arm-assembly-on-android"]

+++

Here's a little demonstration about how to assemble a native library, written in the GNU flavor of the ARMv7 assembly, that will be called through the Java Native Interface, using an Android project as an example.

The procedure will return a Java byte[] array object containing the content of a static string, defined in the library.

In most cases, C/C++ will do a far better job. However, for the record, this document provide informations about how to do that without a C compiler (You'll still need an ARMv7 assembler, though...).

This document complements [Assemble a native ARMv7 library, and call Android Java methods from its procedures invoked by an Android App, using the JNI conventions](/documentation/armv7-assembly-native-library-calling-android-activity-method).

## The example

### Coding the library

This example is heavily commented as I wrote it while learning assembly. This should provide a clear understanding of this example for people new to ARM Assembly.

If you're a professional, you might find it more comfortable to strip the comments with the editor of your choice.

[**wild.s**](https://github.com/Miouyouyou/ARMv7-Java-JNI-return/blob/master/wild.s)

```gas
.data

msg:
  .ascii  "A wild Assembly appears !\n"
msg_len = . - msg

.text
.align 2
.globl Java_your_pack_testactivity_TestActivity_testMe
.type Java_your_pack_testactivity_TestActivity_testMe, %function
Java_your_pack_testactivity_TestActivity_testMe:
  stmfd sp!, {r4-r6, lr} // Prologue. We will use r4 and r6. Is push more useful than stmfd ?
  
  // Useful passed parameters - r0 : *_JNIEnv
  mov r4, r0 // Save *_JNIEnv for the second method

  // Preparing to call NewByteArray(*_JNIEnv : r0, size_of_array : r1). *_JNIEnv is already loaded.
  mov r1, #msg_len   // r1 : size_of_array = msg_len
  ldr r5, [r0]       // Getting NewByteArray : Get *JNINativeInterface from *_JNIEnv. *JNINativeInterface is preserved for later use.
  ldr r3, [r5, #704] // Get *JNINativeInterface->NewByteArray. +704 is NewByteArray 's offset
  blx r3             // r0 : *bytearray <- NewByteArray(*_JNIEnv : r0, size_of_array : r1)
  mov r6, r0         // We need to keep *bytearray elsewhere as it will be returned by our procedure. r0 is needed for *_JNIEnv

  /* Note : Calculting offset in a structure containing only function pointers is equivalent to :
     Number of functions pointers declared before the desired function pointer * Size in bytes of a function address (4 in 32-bit)
  
     Preparing to call *JNativeInteface->SetByteArrayRegion(*_JNIEnv : r0, *bytearray r1, 0 : r2, int bytes_to_copy : r3, *from : sp) */

  mov r1, r0         // r1 : *bytearray - The return value of NewByteArray
  mov r0, r4         // r0 : *_JNIEnv - Previously saved in r4
  mov r2, #0         // r2 : 0 - Define the starting index for the array-copy procedure of SetByteArrayRegion
  mov r3, #msg_len   // r3 : bytes_to_copy = msg_len
  sub sp, sp, #4     // Preparing the stack in which we'll store the address of msg
  ldr r4, =msg       // We won't need our previous copy of *_JNIEnv anymore, so we replace it by *msg.
  str r4, [sp]       // sp : *from = msg address - the native byte array to copy inside the Java byte[] array
  ldr r5, [r5, #832] // r5 <- r5 : *JNativeInterface->SetByteArrayRegion (+832). We don't need r5 after this so we store the function address directly in it.
  blx r5             // SetByteArrayRegion(*_JNIEnv : r0, *bytearray : r1, 0 : r2, size_of_msg : r3, *msg : sp)
  
  add sp, sp, #4        // Get our stack space back !
  mov r0, r6             // *bytearray : Our return value
  ldmfd sp!, {r4-r6, pc} // Restoring the scratch-registers and returning by loading the link-register into the program-counter
```

Then assemble and link this example library :

```bash
export PREFIX="armv7a-hardfloat-linux-gnueabi" # Replace this by the prefix of your toolset or remove '$PREFIX-' from the next commands
export DEST="/path/to/your/TestActivityProject/app/src/main/jniLibs" # Skip this if you don't have an Android project
$PREFIX-as -o wild.o wild.s
$PREFIX-ld.gold -shared --dynamic-linker=/system/bin/linker -shared --hash-style=sysv -o libwildAssembly.so wild.o
cp libwildAssembly.so $DEST/armeabi/libwildAssembly.so # Skip this if you don't have an Android project
cp libwildAssembly.so $DEST/armeabi-v7a/libwildAssembly.so # Skip this if you don't have an Android project
```

### Calling this from Android

Generate a project with :

* the same package name you used in the assembly listing (`your.pack.testactivity`),
* an activity named `TestActivity`

And define `native byte[] testMe()` in it.

[**TestActivity.java**](https://github.com/Miouyouyou/ARMv7-Java-JNI-return/blob/master/apk/app/src/main/java/your/pack/testactivity/TestActivity.java)

```java
package your.pack.testactivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class TestActivity extends AppCompatActivity {

  /* Basically, the android system will look for a "libwildAssembly.so" in the 
     app's private and public folders. */
  static { System.loadLibrary("wildAssembly"); }

  /* And then look for a symbol named :
    Java_package_name_ClassName_methodName.
    
    The current package name is : your.pack.testactivity
    The current class name is : TestActivity 
    The method name is testMe
    So the android linker will look for a symbol named :
    Java_your_pack_testactivity_TestActivity_testMe 
    
    There is no signature or return value check in assembly, so your
    java compiler will compile this class EVEN if the library is not
    there or if the symbol name is invalid.
    There is no such things as "return type" or "parameters type" in 
    assembly so no such check will be performed ever. */
  static native byte[] testMe();
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_test);

    TextView mContentView = (TextView) findViewById(R.id.fullscreen_content);
    mContentView.setText(new String(testMe()));

  }

  /* Try it : Redeclare testMe() as 'native int testMe()' and 
     new String(testMe()) by String.format(Locale.C, "%d", testMe()) */
}
```

[**activity_test.xml**](https://github.com/Miouyouyou/ARMv7-Java-JNI-return/blob/master/apk/app/src/main/res/layout/activity_test.xml)

```xml

<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
             xmlns:tools="http://schemas.android.com/tools"
             android:layout_width="match_parent"
             android:layout_height="match_parent"
             android:background="#0099cc"
             tools:context="your.pack.testactivity.TestActivity"
  >

  <!-- The primary full-screen view. This can be replaced with whatever view
         is needed to present your content, e.g. VideoView, SurfaceView,
         TextureView, etc. -->
  <TextView
    android:id="@+id/fullscreen_content"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:gravity="center"
    android:keepScreenOn="true"
    android:text="@string/dummy_content"
    android:textColor="#33b5e5"
    android:textSize="50sp"
    android:textStyle="bold"
    />

</FrameLayout>
```

### Copy the generated native library inside the Android project

Create a directory named jniLibs in **$YourProjectRootFolder/app/src/main** if it doesn't exist

Then create two directories armeabi and armeabi-v7a in it so you have :

- **$YourProjectRootFolder/app/src/main/jniLibs/armeabi**
- **$YourProjectRootFolder/app/src/main/jniLibs/armeabi-v7a**

Copy your library `libwildAssembly.so` in those folders

Then compile and install the project on your phone, using either :
* `gradle installDebug`
* The 'Run' button on Android Studio

## How it works, basically

For what I understand, when you define the following in a Java class :

```java
package your.package

public class YourClass ... {
  ... {
  System.loadLibrary("name"); 
  }
  ...
  native return_type methodName(parameters...)
  ...
}
```

* The JVM (or Dalvik) will first search for the library "name" in a way typical to the current system.  
  Using the same example, on Android systems (Linux), Dalvik will search for `libname.so` in places referenced by the current `LD_LIBRARY_PATH`.
* Then, it will look for a symbol following this pattern in the library found :  
  `Java_your_package_YourClass_methodName`
* Once the symbol found, it will execute the instructions at the symbol address, passing the following arguments using the standard procedure call convention :
	* the address of the data structure representing the current Java environment (`*_JNIEnv` in C programs) (in **r0** on ARM)
	* the address of the data structure representing the current Java object (`this`) on which the method is called (`jobject thisObj`) (in **r1**)
	* the other arguments (in **r2**, **r3** and the stack accessible through **sp**)

If you look in the `jni.h` file provided with your NDK, you'll see that `_JNIEnv` is a data structure defined like this :

```c
struct _JNIEnv {  
   const struct JNINativeInterface* functions;  
  /* C++ specific hacks around 'functions' */
}
```

The JNINativeInterface is a data structure composed only by function pointers, plus a starting padding (of 4 void* pointers).

So basically, _JNIEnv* equates to :

```c
*_JNIEnv ->
	*JNINativeInterface ->
		NULL
		NULL
		NULL
		NULL
		*GetVersion
		*DefineClass
		...
```

Getting the address offset of a function pointer defined in `JNINativeInterface` tends to boil down to :

`Size of a procedure address (4) * number of statements preceding the statement defining the function pointer`

For example, the offset of `NewByteArray`, preceded by 176 statements, is 176*4 = 704.

Note that [the JNI documentation actually provides the index of each function pointer in JNINativeInterface](https://docs.oracle.com/javase/7/docs/technotes/guides/jni/spec/functions.html).  
If you look at the documentation, you'll see that the index of `NewByteArray` is 176. Then multiply by 4 octets, the size of an address on ARMv7 architectures, and you'll get the same offset : 704.

Since the argument provided by the JNI to the native procedure is a pointer to `_JNIEnv`, calling `NewByteArray` requires to :

* Get the data structure pointed by **r0**
* Get the data structure pointed in result + 704
* Prepare to pass the first parameter in **r1**
* Call the result

However, note that most of the JNI functions require `*_JNIEnv`, so you'll have to save **r0** somewhere in order to call the different functions correctly.

Once you know that, the rest is kinda easy.

Look up the appropriate functions to call in the JNI documentation and call them with the right arguments.
